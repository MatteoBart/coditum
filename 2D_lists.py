'''
Created on Nov 14, 2019

@author: coditum
'''
#2D lists

x = [[1,2,3], [4,5,6]]
print(x[0])
print(x[1])
print(len(x))
#2
print(x[0][1])
#5
print(x[1][1])
print(len(x[0]))


#Inner lists of different sizes
letters = [["A", "B", "C"], ["D", "E"], 
           ["F"]]
print(letters)
for i in range(len(letters)):
    for j in range(len(letters[i])):
        print(letters[i][j])
    print()





