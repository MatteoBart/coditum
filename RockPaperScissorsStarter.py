'''
Created on Oct 24, 2019

@author: coditum
'''

import random #import random here because we will be using random numbers

# think through the code, what is the best way to store someones choice
# boolean doesn’t make sense, float doesn’t make sense, string is okay 
# int is the best
#rock = 0
#paper = 1
#scissors = 2
while True:
    # chose a random number to as the computer turn
    # what does it mean if the number is one, two, or three??
    computers_choice = random.randint(0, 2)
    
    # accept user input as their turn
    # again you want this to store this the same way you store the computer turn
    print("Rock = 0")
    print("Paper = 1")
    print("Scissor = 2")
    users_choice = int(input("Please choose a number: "))
    while users_choice != 0 or users_choice != 1 or users_choice != 2:
        users_choice = int(input("Please choose a valid number: "))

    
    
    if computers_choice == 0:
        print("Computer chose Rock")
    elif computers_choice == 1:
        print("Computer chose Paper")
    elif computers_choice == 2:
        print("Computer chose Scissor")
    
    # then use a bunch of if statements to see who wins the game 
    # this can be done in 9 if statements easily
    # but it can be done with 7 if statements (strive for this one)
    if (computers_choice == users_choice):
        print("Tie")
    elif (computers_choice == 0 and users_choice == 1):
        print("User won")
    elif (computers_choice == 0 and users_choice == 2):
        print("Computer won")
    elif (computers_choice == 1 and users_choice == 0):
        print("Computer won")
    # elif (computers_choice == 1 and users_choice == 1):
    #     print("Tie")
    elif (computers_choice == 1 and users_choice == 2):
        print("User won")
    elif (computers_choice == 2 and users_choice == 0):
        print("User won")
    elif (computers_choice == 2 and users_choice == 1):
        print("Computer won")
    # elif (computers_choice == 2 and users_choice == 2):
    #     print("Tie")