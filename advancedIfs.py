'''
Created on Oct 24, 2019

@author: coditum
'''
'''
# Option A (not as good)
num = 8 
if (num > 0):
    if (num < 11):
        print(num)
        print("this is a small non neg num")
        
        
# Option B (this is the better) 
if (num > 0 and num < 11):
    print(num)
    print("this is a small non neg num")
    
my_num = 0
if (my_num > 0 and my_num < 0):
    print("I have an impossible number")
'''
'''
num = "test"
if(num == 0):
    print("Greater than 0!")
else:
    print("Less than 0!") 
    
'''
    
city = "Hoboken"
if(city == "Hoboken"):
    print("You are in the mile square")
    print("test 123")
elif(city == "Jersey City"):
    print("You are in JC!!")
elif(city == "Weehawken"):
    print("You are north of Hoboken!")
else:
    print("You are in " + city)


