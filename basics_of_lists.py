'''
Created on Oct 30, 2019

@author: coditum
'''
from list_indices import my_list

# print the last element
# display the list nicely
my_list = [10, 20, 40, 50, 0, 10, 10, 10]
print(len(my_list)) #8

print(my_list[7])
print(my_list[len(my_list)-1])

print_count = 0
while print_count < len(my_list):
    print(my_list[print_count])
    print_count += 1
# print the last element
# display the list nicely

my_grocery_list = ["cucumbers", "pickles", "relish"]
print(len(my_grocery_list)) #3
print(my_grocery_list[1])
print(1)
# print the middle element
# which index is it at


is_this_valid = ["cucumbers", 32, True, "hot dog"] #this works but a bad idea
print(len(is_this_valid)) #4
# add 32
# add Hi
# print the first and last elements
is_this_valid.append(32)
is_this_valid.append("Hi")
print(is_this_valid[0])
print(is_this_valid[5])






'''
print(my_list)  
z = 0
while z < 22:   
    my_list.append(22)
    z = z + 1
print(len(my_list))
print(my_list) #can print the list nicely for us
''' 