'''
Created on Dec 5, 2019

@author: coditum
'''
from random import randint
'''
make_board() -> returns board
display_board(board) -> returns None
check_if_bingo(numbers_called, board) -> returns a boolean
mark_spot(board, spot) -> returns newBoard (with a zero potentially)
'''
#initial board
board = [[64, 53, 69, 8, 47], 
         [13, 62, 70, 47, 48], 
         [51, 19, 65, 46, 19], 
         [59, 64, 44, 22, 27], 
         [41, 34, 17, 40, 12]]

#roll a 17
board = [[64, 53, 69, 8, 47], 
         [13, 62, 70, 47, 48], 
         [51, 19, 65, 46, 19], 
         [59, 64, 44, 22, 27], 
         [41, 34, 0, 40, 12]]

#roll a 19
board = [[64, 53, 69, 8, 47], 
         [13, 62, 70, 47, 48], 
         [51, 0, 65, 46, 0], 
         [59, 64, 44, 22, 27], 
         [41, 34, 0, 40, 12]]

#roll a 23
board = [[64, 53, 0, 8, 47], 
         [13, 62, 0, 47, 48], 
         [0, 0, 0, 0, 0], 
         [59, 64, 0, 22, 27], 
         [41, 34, 0, 40, 12]]

#create a random board of size 5x5
#returns a 2D int list
def make_board():
    board = []
    for i in range(5):
        temp = []
        for j in range(5):
            temp.append(randint(1,75))
        board.append(temp)
    return board

#prints the board to the terminal (nicely formatted)
def display_board():

#checks a given board if winner looking at the zeros
#returns a boolean (True if the player won)
def check_if_bingo(board):

#changes the board spot to a zero if matches spot
#returns a new board
def mark_spot(board, spot):
    



