'''
Created on Oct 17, 2019

@author: coditum
'''
# + - / * **
from math import sqrt #must have the import here bc the sqrt function

x = int(input("Enter a number: "))
y = int(input("Enter another number: "))

print(x+y)
print(x-y)
print(x/y)
print(x//y)
print(x*y)
print(x**y)
print(int(sqrt(x)))
