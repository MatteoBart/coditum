'''
Created on Oct 17, 2019

@author: coditum
'''
x = int(input("Enter a number: "))
y = int(input("Enter another number: "))

# + - * /
operation = input("Enter an operation: ")

if operation == "+":
    print(x+y)
if operation == "-":
    print(x-y)
if operation == "*":
    print(x*y)
if operation == "/":
    print(x/y)
    
#EXTRA CREDIT: Where can we add another if statement such that
# we will not get a divide by 0 error