'''
Created on Nov 7, 2019

@author: coditum
'''
lst = [3, 4, 8, 9, 10, 12, 200, 300, 500, 700]
print("Before", lst)
print("For loop without index")
for num in lst:
    num = num * 10 #this does not modify the list
print("After", lst)

print()

print("Before", lst)
print("For loop with index!")
for i in range(len(lst)):
    lst[i] = lst[i] * 10
print("After", lst)