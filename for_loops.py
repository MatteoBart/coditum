'''
Created on Nov 7, 2019

@author: coditum
'''


# for and in are keywords they will NOT EVER CHANGE
# for some_var in some_list
# 
lst = [1, 2, 3, 4]
for num in lst:
    print(num)
     
all_my_fav_words = ["Mississippi", "caterpillar", "door"]
for word in all_my_fav_words:
    print(word + "!")
    
# range(3) => [0, 1, 2] 
print(list(range(10))) 
# range(3, 10) => [3, 4, 5, 6, 7, 8, 9]
print(list(range(3, 10)))
# range(1, 100, 10) => [1, 11, 21, 31, 41, 51, 61, 71, 81, 91]
print(list(range(1, 100, 10)))
# range(10, -10, -2) => [10, 8, 6, 4, 2, 0, -2, -4, -6, -8]
print(list(range(100, -100, -2)))

