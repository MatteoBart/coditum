'''
Created on Oct 17, 2019

@author: coditum
'''

#example 1: ask for their name
z = input("What is your name") 
print("Hello, " + z + " nice to meet you!")

#example 2: ask for their favorite number
their_fav_num = int(input("What is your favorite num?"))
print(type(their_fav_num)) #this should be an int
print("This is your fav num: " + str(their_fav_num)) #convert it back to a string so we can add it to another string

#BIG TAKEAWAY
#INPUT ALWAYS RETURNS A STRING
