'''
Created on Oct 24, 2019

@author: coditum
'''
# While loops

num = 0

while num < 8:
    print(num)
    num += 1
    if num == 3:
        print("You are getting close!")
        
    
print("Got here")
