'''
Created on Oct 17, 2019

@author: coditum
'''

in_hoboken = False

# == means "is equal to"
# if conditon is True then run the code under it

#example 1: just a boolean variable
if in_hoboken: #If in_hoboken is True
    print("Go to coditum!")
    print("Week 2!")
if not in_hoboken: #not will always do the opposite (not True) => False
    print("Go back to hoboken!")

#example 2: using an if with an integer
week = 4
if not week == 2:
    print("It is week 2!")
if week != 2: #! = not
    print("It is not week 2!")
