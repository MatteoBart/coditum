'''
Created on Oct 30, 2019

@author: coditum
'''

my_list = ["Rachel", "Matteo"]
# "Rachel" -> my_list[0]
# "Matteo" -> my_list[1]

#my_num_list = [1,2]
#print(my_num_list[1])


print(len(my_list))
print_count = 0
while print_count < len(my_list):
    print(my_list[print_count])
    print_count += 1

print(my_list)
# print(my_list[1])


my_list = [True, False, True, False]
# length = 4
# index 3 = False
# index 4 = Error
# index 0 = True


