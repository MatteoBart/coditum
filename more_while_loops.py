'''
Created on Oct 30, 2019

@author: coditum
'''

num = 1
while num <= 20 or num == 54 or num > 20:
    if num < 10:
        num = num + 2
        print(num)
        continue
    else:
        break
        
    num = num + 2
    
print("done!")

num = 49
while num != 50:
    num = num + 1
    print("continue!")
    print(num)

print("done again!")