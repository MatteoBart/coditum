'''
Created on Nov 21, 2019

@author: coditum
'''


x = 20
def add20(x): #this x is a local value not the global one defined on line 20
    return x + 20

pi = 3.14 # global variable and can be used inside the functions
def getCircleArea(radius):
    print(radius)
    return pi * radius ** 2 #radius # we use the global variable pi here

# print(radius) #radius is a local variable to the function getCircleArea
print(getCircleArea(3))
print(add20(x))
print(getCircleArea(5))

#print(print(3))

# return statements

def loopyloop(x):
    while x < 10:
        print(x)
        if x == 6:
            return("6!")
        x += 1
        
print("value",loopyloop(2))

    