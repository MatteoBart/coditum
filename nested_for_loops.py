'''
Created on Nov 14, 2019

@author: coditum
'''
# Nested For loops

# Print a Square
for i in range(4):
    for j in range(4):
        print("*", end=" ")
    print()

print()

# Print a right triangle
for i in range(4):
    for j in range(i+1):
        print("*", end=" ")
    print()

