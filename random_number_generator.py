'''
Created on Dec 5, 2019

@author: coditum
'''
from random import randint

#opens a file given the name and what you want to do 
#"w" => write [write WILL destroy old file]
#"r" => read [will not destroy the file]
#"a" => append [will not destroy the file]
file = open("numbers.txt","w") 

# range(start, stop-1, step)
for num in range(0,100): #do this thing 100 times
    x = randint(0,300) #create a random number btw 0 and 300
    file.write(str(x) +"\n") #write to the file with a new line
    
file.close() #don't forget to file close!

    
    
