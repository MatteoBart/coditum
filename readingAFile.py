'''
Created on Dec 5, 2019

@author: coditum
'''
#methods are function inside classes
file = open("numbers.txt", "r") #open the file as reading
lines = file.readlines() # turn the file into a list
numbers = [] #create an empty list
print(lines)
for line in lines: # for each line in the file
    print(line)
    number = int(line.replace("\n", "")) # removes the \n in order to turn the line into an integer
    numbers.append(number) # adds the number to the list

print(numbers)
print("The sum is", sum(numbers)) # sum of all the numbers in the list
print("The max is", max(numbers)) # max element
print("The min is", min(numbers)) # min element
file.close() # closing the file