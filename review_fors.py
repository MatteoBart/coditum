'''
Created on Nov 7, 2019

@author: coditum
'''
# For loops v. While loops

# For loops

#print the numbers 0 to 32
for num in range(0, 33):
    print(num)

#print numbers 100 to 1000
for num in range(100, 1001):
    print(num)
    
# While loops

#print the numbers 0 to 32
x = 0
while x < 33:
    print(x)
    x += 1

#print numbers 100 to 1000
x = 100
while x < 1001:
    print(x)
    x += 1
