'''
Created on Nov 7, 2019

@author: coditum
'''


# while loops

x = 1
while x <= 100:
    print(x)
    x += 1 # x = x + 1
    
# lists

lst = ["a", "b", "c", "d"]

print(len(lst))
print(lst[0])
lst.append("e")
lst.remove("a")
print(lst)