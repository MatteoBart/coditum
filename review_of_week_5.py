'''
Created on Nov 14, 2019

@author: coditum
'''

x = [1, 2, 3, 4, 5, 6]

# Multiply each item in the list by 2
for num in x:
    num *= 2
print(x)
#Changes the value in the list
for i in range(len(x)):
    x[i] *= 2
print(x)

# Shopping cart review
store = ["Apples", "Oranges", "Lemons"]
cart = []

# Add Apples and Oranges
cart.append(store[0])
cart.append(store[1])
print(cart)

# Remove Apples
cart.remove("Apples")

# Print how many items are in the cart
print(len(cart))

# For loop vs While loop Review
# Print numbers 1-50 going up by 2

# For loop
# range(start, stop, step)
for i in range(1,51, 2):
    print(i)
# While loop
x = 1
while x<51:
    print(x)
    x+=2







