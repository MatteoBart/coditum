'''
Created on Nov 21, 2019

@author: coditum
'''

# Nested loops
# 2D Lists

#Sum up all of the values in the list using nested loops

x = [[1,2,3], [4,5,6]]

sum = 0
for i in range(2):
    for j in range(3):
        sum += x[i][j]
print(sum)

#Sum up all of the values in the list using nested loops

y = [[1,2,3], [4,5], [6], [4, 5, 5, 7]]

sum = 0
for i in range(len(y)):
    for j in range(len(y[i])):
        sum += y[i][j]
print(sum)

sum = 0
for l in y:
    print(l)
    for e in l:
        print(e) 
        sum += e
print(sum)

