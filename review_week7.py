'''
Created on Dec 5, 2019

@author: coditum
'''

def add(x, y, z):
    return x + y + z

a = 3
#print(add(a+3, a, a))

#y = None 
#z = print("hello")
#print(z)

def doNothing(a):
    q = a + 3
    return None
    
z = doNothing(3)
print(z)