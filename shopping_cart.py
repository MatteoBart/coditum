'''
Created on Nov 7, 2019

@author: coditum
'''

store = ["Bread", "Eggs", "Milk", "Almond Milk", "Almonds", "Cashews", "Ice cream"]

'''
len(store) = 3
bread eggs milk
0      1     2
'''

cart = []

print("Welcome to our store!")

add_another = True
while add_another:
    for i in range(len(store)):
        print(str(i)+")",store[i])
    
    add_or_remove = int(input("Would you like to add or remove an item? (1) Add (2) Remove"))
    if add_or_remove == 1:
        item = int(input("Which item would you like to add to your cart?"))
        cart.append(store[item])
    elif add_or_remove == 2 and cart != []:
        item = int(input("Which item would you like to remove from your cart?"))
        cart.remove(store[item])
    
    print(cart)
    again = int(input("Do you want to continue to shop? (1) Yes (2) No"))
    if again == 2:
        add_another = False
        
print("Thanks for shopping with us!")
