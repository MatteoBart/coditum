'''
Created on Dec 12, 2019

@author: coditum
'''
import plotly.graph_objects as go

f = open("data.csv", "r")
lines = f.readlines()
dates = []
values = []
for line in lines[1:]:
    fline = line.split(",")
    dates.append(fline[0])
    values.append(float(fline[1].replace("\n","")))

fig = go.Figure(data=go.Scatter(x=dates, y=values))
fig.show()