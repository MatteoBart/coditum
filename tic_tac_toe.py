'''
Created on Nov 14, 2019

@author: coditum
'''
board = []
for i in range(3):
    temp = []
    for j in range(3):
        temp.append(" ")
    board.append(temp)

count = 0
letter = "X"
while count < 9 and count >= 0:
    print(" "+board[0][0]+" | "+board[0][1]+" | "+board[0][2]+" ")
    print(" "+board[1][0]+" | "+board[1][1]+" | "+board[1][2]+" ")
    print(" "+board[2][0]+" | "+board[2][1]+" | "+board[2][2]+" ")
    
    i = int(input("Enter your i coordinate"))
    j = int(input("Enter your j coordinate"))
    
    board[i][j] = letter
    count += 1
    
    if board[0][0]==letter and board[0][1]==letter and board[0][2]==letter:
        print(letter+" wins!")
        count = -1
    elif board[1][0]==letter and board[1][1]==letter and board[1][2]==letter:
        print(letter+" wins!")
        count = -1
    elif board[2][0]==letter and board[2][1]==letter and board[2][2]==letter:
        print(letter+" wins!")
        count = -1
    elif board[0][0]==letter and board[1][0]==letter and board[2][0]==letter:
        print(letter+" wins!")
        count = -1
    elif board[0][1]==letter and board[1][1]==letter and board[2][1]==letter:
        print(letter+" wins!")
        count = -1
    elif board[0][2]==letter and board[1][2]==letter and board[2][2]==letter:
        print(letter+" wins!")
        count = -1
    elif board[0][0]==letter and board[1][1]==letter and board[2][2]==letter:
        print(letter+" wins!")
        count = -1
    elif board[0][2]==letter and board[1][1]==letter and board[2][0]==letter:
        print(letter+" wins!")
        count = -1
    
    if letter == "X":
        letter = "O"
    elif letter == "O":
        letter = "X"

if count == 9:
    print("It's a tie!")
print("The end")
    
        