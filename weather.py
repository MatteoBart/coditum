import requests

key = "ce18117ec64332512e0dbf8724514238"
zipcode = input("What zipcode would you like to know the weather for? ")
options = input("How would you like your weather \na) Cel b) Far c) Kelvin [Default]? ")

url = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipcode + "&APPID=" + key

if options == "a":
  url = url + "&units=metric"
  symbol = "c"
elif options == "b":
  url = url + "&units=imperial"
  symbol = "f"
else:
  symbol = "K"

r = requests.get(url)  #actually pull the data

dict = r.json()  #decode into a python dict to use properly
print(dict)
name = dict["name"]
temp = dict["main"]["temp"]
condition = dict["weather"][0]["description"]

print("In", name, "it is currently", temp, symbol, "and you should see",
      condition)
