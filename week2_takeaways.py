'''
Created on Oct 17, 2019

@author: coditum
'''

#there should always be a variable on the right side
#of an input statement
z = input("What is your name?") #DO THIS

input("What is your name") #NOT THAT

#in an if statement you ALWAYS use a double equal sign
y = 3 #use a single equal sign here
if y == 3: #use a double equal sign here
    print("y is equal to 3")
#a single equal sign is the assignment operation
#a double equal sign checks for equality (checks if two things 
#are the same)

#the anatomy of an if statement
#if BOOLEAN:
#    #whatever the code is here


