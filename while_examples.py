'''
Created on Oct 30, 2019

@author: coditum
'''
x = 0 #starting 
while x < 10: #condition 
    print(x) #do something
    x=x+1 #increment
     

z = 120 #start at 120
while z <= 200: #go up to 200
    print(z)
    z = z + 1 #increment by one
print("I am out of the loop my value is: ")
print(z)

''' 
 start at 44
 go up by 2 
 stop at 200
'''
start = 44
while start < 200:
    print(start) #do something
    start = start + 2

'''
start 5
go up by 1
end at 10 (including 10)
'''
x = 5 #start at 5
while x <= 10:
    print(x)
    x = x + 1

'''
start at 100
go up by 10
stop at 1000
'''
z = 100
while z < 1000:
    print(z)
    z = z + 10

'''
start at 100
go down by 2
stop at -100
'''
y = 100 
while y >= -100:
    print(y)
    y = y - 2